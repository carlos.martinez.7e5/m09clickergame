using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickerManager : MonoBehaviour
{
    public int score; //De momento as�, lo suyo seria guardarlo en un Scriptable Object
    int multiplier;
    [SerializeField] int multiplierDuration, multiplierValue;

    AudioSource aSource;
    [SerializeField] AudioClip audioClip;


    [SerializeField] Text scoreTxt;

    private void Start()
    {
        multiplier = 1;
        scoreTxt.text = score.ToString();
        aSource = GetComponent<AudioSource>();
    }

    public void click()
    {
        score += multiplier * 1;
        aSource.PlayOneShot(audioClip);
        updateScore();
    }

    IEnumerator boost()
    {
        multiplier = multiplierValue;
        yield return new WaitForSeconds(multiplierDuration);
        multiplier = 1;
    }

    public void multiplierBoost()
    {
        StartCoroutine(boost());
    }

    public void TestButton()
    {
        Debug.Log("He sido pulsado");
    }

    void updateScore()
    {
        scoreTxt.text = score.ToString();
    }
}
