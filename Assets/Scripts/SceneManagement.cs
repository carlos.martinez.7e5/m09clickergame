using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{
    [SerializeField] GameObject login, singin;

    public void loadScene(string scene)
    {
        SceneManager.LoadScene(scene); 
    }

    public void changePanel()
    {
        login.active = !login.active;
        singin.active = !singin.active;
    }
}
